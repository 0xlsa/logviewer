# Log Viewer
Simple SHELL script to run using `sudo` to give a regular user a way to visualise logs under /var/log.

---

```sh
usage: log.sh [-h] [-v] [follow FILEPATH] [list <FILENAME>] [view FILEPATH]

Simple script to visualise logs under /var/log

optional arguments:
  -h, --help          show this help message and exit
  -v, --version       show program's version number and exit

commands:
    follow            view a log file appending its output as the file grows
    list              list of logs available
    view              view a log file
```