#!/bin/bash

readonly AUTHOR="Luciano Sa"

readonly LOGPATH="/var/log"
readonly VERSION="1.0"

readonly SCRIPT_NAME=$(basename ${0})
readonly ARGS="${*}"

readonly AUDIT_LOGFILE=/dev/log-viewer.log


# Making sure _audit always run
trap "echo ; _audit; exit 130" INT
trap "_audit; exit 143" TERM
trap "_audit" EXIT


function print_usage() {
    printf "
usage: ${SCRIPT_NAME} [-h] [-v] [follow FILEPATH] [list <FILENAME>] [view FILEPATH]

Simple script to visualise logs under ${LOGPATH}

optional arguments:
  -h, --help          show this help message and exit
  -v, --version       show program's version number and exit

commands:
    follow            view a log file appending its output as the file grows
    list              list of logs available
    view              view a log file
"
}


function _audit() {
    local user_running=${SUDO_USER:-${USER}}

    # some shells will call EXIT after the INT handler
    trap "" EXIT

    if [ -w "${AUDIT_LOGFILE}" ]; then
        echo "$(date +'%Y/%m/%d %H:%M:%S') [${user_running}] - [${EXIT_CODE:-0}]: ${SCRIPT_NAME} ${ARGS}" >> "${AUDIT_LOGFILE}"
    fi
}


function _print() {
    echo -e "${*}"
}


function _print_error() {
    _print "ERROR: ${*}" >&2
}


function _is_text_file() {
    file "${1}" 2> /dev/null | cut -d":" -f2 | grep -Eqw "(ASCII|text)"
    return ${?}
}


function _validate_filepath() {
    local filepath="${1}"
    if [ -z "${filepath}" ]; then
        _print_error "logpath not specified"
        return 1
    fi

    if ! _is_text_file "${filepath}"; then
        _print_error "logfile ${filepath} is not a text file"
        return 2
    fi
}


function _normalise_filepath() {
    # Add LOGPATH to filepath if not present.
    # I.e. path/to/file -> /var/log/path/to/file

    local filepath="${1#/}"
    if [ -n "${filepath}" ]; then
        filepath="${LOGPATH}/${filepath#${LOGPATH}/}"
        echo "${filepath}"
    fi
}


function follow() {
    local filepath="`_normalise_filepath ${1}`"
    _validate_filepath "${filepath}" && tail --follow "${filepath}"
}


function view() {
    local filepath="`_normalise_filepath ${1}`"

    # When the environment variable LESSSECURE is set to 1, less runs in a "secure" mode.
    _validate_filepath "${filepath}" && LESSSECURE=1 less --force "${filepath}"
}


function list() {
    local filter="${1}"
    local rv=2
    for filepath in $(find "${LOGPATH}" -type f 2> /dev/null); do
        _is_text_file "${filepath}" || continue
        basename "${filepath}" | grep -Fq "${filter}" || continue
        # stripping off LOGPATH.
        # I.e. /var/log/path/to/file -> path/to/file
        echo "${filepath#${LOGPATH}/}"
        rv=0
    done

    if [ ${rv} -ne 0 ] && [ -n "${filter}" ]; then
        _print_error "not found any file containing \"${filter}\""
    fi

    return ${rv}
}


# Only root/sudo can run me
if [ ${UID} -ne 0 ]; then
    _print_error "you have not enough priviledges to run this script"
    exit -1
fi


CMD="${1}"
shift 1


# List of valid commands
case "${CMD}" in
    follow)
        follow ${*}
        ;;
    list|ls)
        list ${*}
        ;;
    view)
        view ${*}
        ;;
    -v|--version)
        _print "${SCRIPT_NAME} v${VERSION}\n\nWritten by ${AUTHOR}"
        ;;
    -h|--help)
        print_usage
        ;;
    *)
        error_msg="invalid option ${CMD}"
        if [ -z "${CMD}" ]; then
            error_msg="no option provided"
        fi
        _print_error "${error_msg}"
        print_usage
        EXIT_CODE=1
        ;;
esac


EXIT_CODE=${EXIT_CODE:-${?}}
exit ${EXIT_CODE}
